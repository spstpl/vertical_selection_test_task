import React,{ Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";

class DatePickerDemo extends Component{
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {
            selectsEnd,
            selectsStart,
            handleEndDate,
            handleStartDate,
            endDate,
            startDate
        } = this.props;
        return (
        <DatePicker
                className='date_picker_class form-control'
            selectsStart={selectsStart}
            selectsEnd={selectsEnd}
            selected={selectsEnd ? endDate : startDate}
            onChange={selectsEnd 
                ? handleEndDate
                : handleStartDate
            }
            startDate={!selectsEnd && startDate}
            endDate={selectsEnd && endDate}
            minDate={new Date(moment().add(21, 'day').format())}
            dateFormat="d MMMM yyyy"
        />
        );
    }
}

export default DatePickerDemo;