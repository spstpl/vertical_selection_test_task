import React, { useState, useEffect } from 'react';
import DatePickerDemo from '../DatePickerDemo';
import {
  Label,
  Button,
  Col,
  Row,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Input,
} from 'reactstrap';
import moment from 'moment';
import './style.css';

const Home = () => {
  const localDate = new Date(moment().add(21, 'day').format());
  const [startDate, setStartDate] = useState(localDate);
  const [endDate, setEndDate] = useState(localDate);
  const [otherSelected, setOtherSelected] = useState(false);
  const [threshold, setThreshold] = useState('');

  useEffect(() => {
    if (moment(startDate).format('MM/DD/YY')
        > moment(endDate).format('MM/DD/YY')) {
      setEndDate('');
    }
  }, [startDate, endDate]);

  const handleOtherButton = () => {
    setOtherSelected(true);
    console.log('Rainfall Threshold', threshold);
  }
  const handleStartDate = (date) => {
    setStartDate(date);
  }
  const handleEndDate = (date) => {
    setEndDate(date);
  }
  return (
    <div className="datecontent">
      <Row>
        <Col md="12">
          <h4>Time Period</h4>
        </Col>
      </Row>
      <Row>
        <Col md="6">     
          <div className="form-group">
            <Label className="form-label">From</Label>          
            <DatePickerDemo
              selectsStart
              selectsEnd={false}
              startDate={startDate}
              endDate={endDate}
              handleStartDate={handleStartDate}
              />
          </div>  
        </Col>
        </Row>
        <Row>
        <Col md="6">      
          <div className="form-group">
          <Label className="form-label">To</Label>         
          <DatePickerDemo
            selectsEnd
            selectsStart={false}
            endDate={endDate}
            startDate={startDate}
            handleEndDate={handleEndDate}            
            />
          </div>
        </Col>
      </Row>

      <h4 className="mt-4">Rainfall Thresholds</h4>
      <Row>
        <Col md="6">
          <Button color="link" className="btn-1" block>
            25mm or above
          </Button>
        </Col>
        <Col md="1" className="d-flex align-items-center" >
          <i className="fas fa-info-circle" />
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md="6">
          <Button color="link" className="btn-2" block>
            40mm or above
          </Button>
        </Col>
        <Col md="1" className="d-flex align-items-center" >
          <i className="fas fa-info-circle" />
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md="6">
          <Button color="link" className="btn-3" block>
            80mm or above
          </Button>
        </Col>
        <Col md="1" className="d-flex align-items-center" >
          <i className="fas fa-info-circle" />
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md="6">
          <Button
            color="link" className="btn-4"
            onClick={handleOtherButton}
            block
            default
          >
            Other
          </Button>
        </Col>
        <Col md="1" className="d-flex align-items-center" >
          <i className="fas fa-info-circle" />
        </Col>
        <Col md="3">
          {otherSelected &&
            <InputGroup className="groupInputSec">
              <Input
                value={threshold}
                className="groupInputText"
                placeholder="35"
                onChange={(e) => {
                  setThreshold(e.target.value);
                  console.log('Rainfall Threshold', e.target.value);
                }}
              />
              <InputGroupAddon addonType="append" >
              <InputGroupText className="inputGroupAdjust">mm</InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          }
        </Col>
      </Row>
    </div>
  )
}

export default Home;